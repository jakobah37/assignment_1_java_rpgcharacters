package Objects;

public class Weapon extends Item {
    public enum WeaponType {
        Axe,
        Bow,
        Dagger,
        Staff,
        Hammer,
        Sword,
        Wand
    }
    String weaponSlot; //Måske det skal laves om til weaponSlot i stedet for String.
    WeaponType weaponType;
    int level, dmg;
    double AS; //AS = Attack Speed

    public Weapon() {
        super();
    }

    public void setWeaponSlot(String weaponSlot) {
        this.weaponSlot = weaponSlot;
    }

    public String getWeaponSlot() {
        return this.weaponSlot;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public WeaponType getWeaponType() {
        return this.weaponType;
    }

    public void setDmg(int dmg) {
        this.dmg = dmg;
    }

    public int getDmg() {
        return this.dmg;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return this.level;
    }

    public void setAS(double as) {
        this.AS = as;
    }

    public double getAS() {
        return this.AS;
    }
}
