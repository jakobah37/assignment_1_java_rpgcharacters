package Objects;

public enum ArmorType {
    Cloth,
    Leather,
    Mail,
    Plate
}
