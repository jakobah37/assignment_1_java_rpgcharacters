package Objects;

public class Armor extends Item {
    private Armor.ArmorType ArmorType;

    public enum ArmorType {
        Cloth,
        Leather,
        Mail,
        Plate
    }
    String Armorslot; //Måske det skal laves om til weaponSlot i stedet for String.
    int level;

    public Armor() {
        super();
    }

    public void setArmorslot(String armorSlot) {
        this.Armorslot = armorSlot;
    }

    public String getArmorslot() {
        return this.Armorslot;
    }

    public void setArmorType(ArmorType armorType) {
        this.ArmorType = armorType;
    }

    public ArmorType getArmorType() {
        return this.ArmorType;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return this.level;
    }

}
