package Objects;

// Abstract class
abstract class Item {
    String name, itemSlot;
    int reqLevel;

    // Method with common attributes for items.
    public Item() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setItemSlot(String itemSlot) {
        this.itemSlot = itemSlot;
    }

    public String getItemSlot() {
        return this.itemSlot;
    }

    public void setReqLevel(int reqLevel) {
        this.reqLevel = reqLevel;
    }

    public int getReqLevel() {
        return this.reqLevel;
    }

}


// Subclass (inherit from Objects.Item)






