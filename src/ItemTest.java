import Objects.Weapon;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
    Weapon weapon = new Weapon();

    @org.junit.jupiter.api.Test
    void setName() {
        String name = "Mega Axe";
        weapon.setName(name);

        assertEquals(weapon.getName(), name);
    }


    @org.junit.jupiter.api.Test
    void setItemSlot() {
        weapon.setItemSlot(SlotType.Weapon.name());

        assertEquals(weapon.getItemSlot(),
                SlotType.Weapon.name());

    }

    @org.junit.jupiter.api.Test
    void setReqLevel() {
        int level = 2;
        weapon.setLevel(level);

        assertEquals(weapon.getLevel(), level);

    }


}