import Objects.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {

    @Test
    void setWeaponSlot() {
        Weapon weapon = new Weapon();
        String slot = SlotType.Weapon.name();
        weapon.setWeaponSlot(slot);

        assertEquals(slot, weapon.getWeaponSlot());
    }


    @Test
    void setWeaponType() {
        Weapon weapon = new Weapon();
        Weapon.WeaponType axe = Weapon.WeaponType.Axe;
        Weapon.WeaponType bow = Weapon.WeaponType.Bow;
        Weapon.WeaponType dagger = Weapon.WeaponType.Dagger;
        Weapon.WeaponType staff = Weapon.WeaponType.Staff;
        Weapon.WeaponType hammer = Weapon.WeaponType.Hammer;
        Weapon.WeaponType sword = Weapon.WeaponType.Sword;
        Weapon.WeaponType wand = Weapon.WeaponType.Wand;

        assertEquals(axe, weapon.getWeaponType());
        assertEquals(bow, weapon.getWeaponType());
        assertEquals(dagger, weapon.getWeaponType());
        assertEquals(staff, weapon.getWeaponType());
        assertEquals(hammer, weapon.getWeaponType());
        assertEquals(sword, weapon.getWeaponType());
        assertEquals(wand, weapon.getWeaponType());

    }


    @Test
    void setDmg() {
        Weapon axe = new Weapon();
        int dmg = 2;
        axe.setDmg(dmg);

        assertEquals(dmg, axe.getDmg());
    }


    @Test
    void setLevel() {
        Weapon weapon = new Weapon();
        int lvl = 3;
        weapon.setLevel(lvl);

        assertEquals(lvl, weapon.getLevel());
    }

    @Test
    void setAS() {
        Weapon weapon = new Weapon();
        double as = 37.5;
        weapon.setAS(as);

        assertEquals(as, weapon.getAS());
    }

}