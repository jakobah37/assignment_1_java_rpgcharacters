package Character;

import Attributes.BasePrimaryAttributes;

public class Ranger extends character {


    public Ranger(String name) {
        setName(name);
        setBasePrimaryAttributes(new BasePrimaryAttributes(1, 7, 1));
    }



    public void attributeGain() {
        getBasePrimaryAttributes().addAttributes(new BasePrimaryAttributes(1,5,1));

    }


}