package Character;

import Attributes.BasePrimaryAttributes;

public abstract class character {
     String name;
     int lvl = 1;

    protected Attributes.BasePrimaryAttributes basePrimaryAttributes;


     public character() {

     }

     public String getName() {
         return name;
     }

     protected void setName(String name) {
         this.name = name;
     }

     public int getLvl() {
         return lvl;
     }

     public void lvlUp() {
         lvl ++;
         attributeGain();
     }

     protected void setBasePrimaryAttributes(BasePrimaryAttributes bpm){
         this.basePrimaryAttributes = bpm;
     }

     public BasePrimaryAttributes getBasePrimaryAttributes() {
         return basePrimaryAttributes;
     }
     public abstract void attributeGain();

 }








