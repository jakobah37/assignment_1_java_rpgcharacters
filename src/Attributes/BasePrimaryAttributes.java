package Attributes;

public class BasePrimaryAttributes {
    private int strength;
    private int dexterity;
    private int intelligence;

    public BasePrimaryAttributes(int strength, int dexterity, int intelligence){
        setStrength(strength);
        setDexterity(dexterity);
        setIntelligence(intelligence);
    }

    public void addAttributes(BasePrimaryAttributes adder){ // Credits Marius Samson
        setStrength(getStrength()+adder.getStrength());
        setDexterity(getDexterity()+adder.getDexterity());
        setIntelligence(getIntelligence()+adder.getIntelligence());//Tager nuværende strength værdi og lægger adder strength værdi til.

    }


    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

}

